import os
import sys
import time
from queue import Queue
import logging
import threading

from common import scheduler, bot_brain
from common.graceful_killer import GracefulKiller


LOG_FILE = os.getenv('LOG_FILE', 'rude_bot.log')
handler = logging.FileHandler(LOG_FILE)

handler_console = logging.StreamHandler(sys.stdout)
handler_console.addFilter(lambda r: r.levelno >= logging.INFO)

logger = logging.getLogger('rude_bot')
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG,
    handlers=[handler, handler_console]
)

logger.info('script restarted.')


def main():
    killer = GracefulKiller()
    bot = bot_brain.Bot()
    bot_msg_query = Queue()
    threads = []
    
    thread = threading.Thread(target=scheduler.regular_interval_query_worker,
                              args=(scheduler.take_some_msg_from_schedule, bot_msg_query),
                              kwargs=dict(interval=10, killer=killer))
    thread.start()
    threads.append(thread)

    thread = threading.Thread(target=bot.say_all_messages_from_query,
                              args=(bot_msg_query,),
                              kwargs=dict(killer=killer))
    thread.start()
    threads.append(thread)
    
    while True:
        if killer.kill_now:
            logger.info('kill_now is True - stopping main loop.')
            break
        time.sleep(2)

    bot_msg_query.put(None)
    for thread in threads:
        thread.join()
        

if __name__ == '__main__':
    main()
