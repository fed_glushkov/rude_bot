from datetime import datetime, timedelta


def round_time(dt, round_to=1):
    if type(dt) is not datetime:
        raise Exception('Expected dt to be of type datetime, instead got: {}'.format(type(dt).__name__))
    if int(round_to) != round_to:
        raise Exception('Expected round_to to be integer number of seconds, instead got: {}'.format(round_to))
    dt, round_to = dt - timedelta(0, 0, dt.microsecond), int(round_to)
    sec = (dt.replace(tzinfo=None) - dt.min).total_seconds()
    delta = round(sec / round_to) * round_to - sec
    return dt + timedelta(0, delta)
