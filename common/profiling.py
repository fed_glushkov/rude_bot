from datetime import datetime
import functools


def call_time(func):
    """Этот декоратор пишет в консоль сколько секунд занял вызов функции func.
    """
    fname = func.__name__ if hasattr(func, '__name__') else ''

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        t = datetime.now()
        r = func(*args, **kwargs)
        t = (datetime.now() - t).total_seconds()
        if t <= 0.99:
            print('{:>3.0f} ms:'.format(t * 1000), fname)
        elif t <= 99:
            print('{:>4.1f} sec:'.format(t), fname)
        elif t <= 99 * 60:
            print('{:>4.1f} min:'.format(t / 60), fname)
        else:
            print('{:>4.1f} hou:'.format(t / 3600), fname)
        return r
    return wrapper
