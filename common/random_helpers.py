"""Функции-помощники для работы с random и np.random.
"""

import numpy as np


def random_state(state=None):
    """ Хелпер-функция для работы с random_state аргументом.
        Код взят из pandas.core.common._random_state

    Parameters
    ----------
    state : int, np.random.RandomState, None.
        If receives an int, passes to np.random.RandomState() as seed.
        If receives an np.random.RandomState object, just returns object.
        If receives `None`, returns np.random.
        If receives anything else, raises an informative ValueError.
        Default None.

    Returns
    -------
    np.random.RandomState
    """
    if type(state) is int:
        return np.random.RandomState(state)
    elif isinstance(state, np.random.RandomState):
        return state
    elif state is None:
        return np.random
    else:
        raise ValueError("random_state must be an integer, a numpy RandomState, or None")


def pick_rand_element_from_iterable(values, rand_state=None):
    if len(values) == 0:
        return np.nan
    if len(values) == 1:
        return tuple(values)[0]
    rand = random_state(rand_state)
    return rand.choice(tuple(values))
