"""Всякая ерунда помогающая работать с модулями, например их перезагружать.
"""

import importlib


def all_local_modules(locals_dict):
    """ Возвращает список всех модулей, в данный момент загруженных в locals_dict.
        Чтобы получить список глобальных модулей, например, в jupyter notebook - передай globals() в качестве аргумента locals_dict.
    """
    return [lv for lv in locals_dict.values() if type(lv).__name__ == 'module']


def reload_all_modules(locals_dict, exc=None):
    """ Перезагружает все модули из locals_dict, за исключением builtins и модулей, переданных в списке exc.
        Чтобы перезагрузить глобальные модули, например, в jupyter notebook - передай globals() в качестве аргумента locals_dict.
    """
    except_modules = [] if exc is None else list(exc)
    for mod in all_local_modules(locals_dict):
        try:
            if mod is not None and mod not in except_modules and mod.__name__ != 'builtins':
                importlib.reload(mod)
        except AttributeError as ex:
            if str(ex) == "'NoneType' object has no attribute 'name'":
                print('module is no longer available and needs to be deleted: {}'.format(mod.__name__))
            else:
                raise
    print('all modules reloaded!')
