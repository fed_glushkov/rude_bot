import numpy as np
import pandas as pd
from common import random_helpers


class RipeningSampler:
    """ Random sampler со сниженной вероятностью повторного выпадения сампла X, если он уже был недавно.
        Чем выше olds_insistency, тем настойчивее старые самплы (которые "ждут" дольше всех) будут требовать своей очереди.
    Пример использования:
        sampler = RipeningSampler(10, olds_insistency=6)
    """
    
    def __init__(self, items_amount, weights=None, olds_insistency=5, forbid_repeats=False, random_state=None):
        self.am = items_amount
        self.items_arr = np.arange(items_amount)
        self._olds_insistency = olds_insistency
        self.forbid_repeats = forbid_repeats
        self.rand = random_helpers.random_state(random_state)

        self._weights = None
        self._ripening_rate = None
        self.set_weights(np.ones(items_amount, dtype=int) if weights is None else weights)
        
        self.activations = None
        self._reset_all_activations()
    
    def set_weights(self, weights):
        if len(weights) != self.am:
            raise Exception('Expected weights to have length equal to self.am, instead got: {}'.format(len(weights)))
        weights = np.array(weights)
        if (weights <= 0).any():
            raise Exception('Expected all weights to be greater than 0, instead got: {}'.format(weights))
        self._weights = weights
        self._recalculate_ripening_rate()
    
    def _recalculate_ripening_rate(self):
        intensity = self._olds_insistency
        if intensity < 0:
            raise Exception(
                'Expected self._olds_insistency to be greater or equal to 0, instead got {}'.format(intensity))
        intensity = 10 ** intensity
        self._ripening_rate = intensity ** (1 / self._weights.sum())
    
    def _reset_all_activations(self):
        a = self._ripening_rate ** np.arange(self.am)
        self.rand.shuffle(a)
        self.activations = a
    
    def give_probs(self):
        return self.activations / self.activations.sum()
    
    def _grow_activations(self):
        a = self.activations
        a = np.maximum(a, self._weights)  # item's initial activation must always be equal to it's weight
        a = a * self._ripening_rate ** self._weights
        self.activations = a
    
    def _reset_item_activation(self, item):
        initial_activation = 0 if self.forbid_repeats else self._weights[item]
        self.activations[item] = initial_activation
    
    def give_next(self):
        item = self.rand.choice(self.items_arr, p=self.give_probs())
        self._grow_activations()
        self._reset_item_activation(item)
        return item
    
    def give_n_next(self, n=None):
        return np.array([self.give_next() for _ in range(n)])


class PhraseManager:
    def __init__(self, items_arr, weights_arr=None, olds_insistency=5, random_state=None):
        am = len(items_arr)
        weights_arr = np.ones(am, dtype=int) if weights_arr is None else weights_arr
        self.df = pd.DataFrame({'item': items_arr, 'weight': weights_arr})
        self.rs = RipeningSampler(am, weights=weights_arr, olds_insistency=olds_insistency, random_state=random_state)
        self._update_activations_from_rs()
    
    def _update_activations_from_rs(self):
        self.df['activation'] = self.rs.activations
    
    def give_next(self):
        row = self.df['item'].iloc[self.rs.give_next()]
        self._update_activations_from_rs()
        return row
    
    def give_n_next(self, n=None):
        rows = self.df['item'].iloc[self.rs.give_n_next(n)]
        self._update_activations_from_rs()
        return rows
    
    def add_items(self, items, weights=None, activations=None, permit_duplicates=False):
        weights = np.ones(len(items), dtype=int) if weights is None else weights
        activations = self.rs.rand.choice(self.df['activation'], len(items)) if activations is None else activations
        if not permit_duplicates:
            duplicates = set(items) & set(self.df['item'])
            if len(duplicates) > 0:
                raise Exception('Expected items to not contain duplicates, instead got: {}'.format(list(duplicates)))
        new_df_frag = pd.DataFrame({'item': items, 'weight': weights, 'activation': activations})
        self.df = pd.concat([self.df, new_df_frag], ignore_index=True)
        self.items_modified()
    
    def add_items_df(self, df):
        new_weights = df['weight'] if 'weight' in df.columns else None
        new_activations = df['activation'] if 'activation' in df.columns else None
        self.add_items(df['item'], weights=new_weights, activations=new_activations)
    
    def remove_items(self, indices):
        self.df.drop(indices, axis=0, inplace=True)
        self.df.reset_index(drop=True, inplace=True)
        self.items_modified()
    
    def items_modified(self):
        """Recalculates RipeningSampler params. Expects, that all 3 df cols are correct."""
        new_am = len(self.df)
        if new_am != self.rs.am:
            self.rs.am = new_am
            self.rs.items_arr = np.arange(new_am)
        self.rs.set_weights(self.df['weight'])
        self.rs.activations = self.df['activation'].as_matrix()
    
    def set_item_activation(self, ind, activation):
        self.df.loc[self.df.index[ind], 'activation'] = activation
        self.rs.activations = self.df['activation'].as_matrix()


def give_gaps(units_amount, game_history):
    bad_items = set(game_history) ^ set(range(units_amount))
    if len(bad_items) > 0:
        raise Exception('Expected set of hist to be equal to set of units, instead got bad items: {}'.format(bad_items))
    last_ind_of_unit = {}
    gaps = []
    for i, unit in enumerate(game_history):
        if unit in last_ind_of_unit:
            gaps.append(i - last_ind_of_unit[unit])
        last_ind_of_unit[unit] = i
    return gaps
