import time
import logging
from datetime import datetime

from common import datetime_helpers


logger = logging.getLogger('rude_bot.scheduler')


def regular_interval_worker(work, interval=60, killer=None):
    logger.info('starting regular_interval_worker loop with interval {}'.format(interval))
    while True:
        if killer is not None and killer.kill_now:
            logger.info('kill_now is True - stopping regular_interval_worker loop.')
            break
        t = datetime_helpers.round_time(datetime.now(), round_to=interval)
        work(t)
        passed = (datetime.now() - t).total_seconds()
        if passed >= interval:
            logger.warning('More than one interval has passed, the loop has been clogged!')
            continue
        if passed * 2 >= interval:
            logger.warning('Half of the interval has passed, there is a risk clogging the loop!')
        time.sleep(interval - passed)
        
        
def regular_interval_query_worker(produce_item, results_query, interval=60, killer=None):
    def put_item_in_results(t):
        item = produce_item(t)
        logger.debug('produced item for t {}: {}'.format(t.strftime('%H:%M:%S'), item))
        if item is not None:
            results_query.put(item)
    regular_interval_worker(put_item_in_results, interval=interval, killer=killer)
    

def take_some_msg_from_schedule(t):
    return '{phrase}' if t.second == 0 else None
