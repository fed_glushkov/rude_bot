import signal
import logging


logger = logging.getLogger('rude_bot.killer')


class GracefulKiller:
    kill_now = False

    def __init__(self):
        logger.debug('creating GracefulKiller instance...')
        for sig in [signal.SIGINT, signal.SIGTERM, signal.SIGBREAK]:
            signal.signal(sig, self.exit_gracefully)
        logger.debug('GracefulKiller instance created, terminating signals handlers added.')

    def exit_gracefully(self, signum, frame):
        logger.debug('caught terminating signal, assigning True to self.kill_now...')
        self.kill_now = True
