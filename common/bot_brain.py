import numpy as np
import logging


logger = logging.getLogger('rude_bot.brain')


def _replace_all_occurrences(s, target, replacement_gen):
    while target in s:
        s = s.replace(target, replacement_gen(), 1)
    return s


class Bot:
    def __init__(self, phrases_arr=None, bastards_arr=None, adjective_arr=None):
        logger.debug('instance of Bot was created')
        self.phrases_arr = phrases_arr or ['Вы не прекращаете поражать меня, {bast}... Своей глупостью.']
        self.bastards_arr = bastards_arr or ['{adj} ублюдки', '{adj} организмы', 'мясные мешки']
        self.adjective_arr = adjective_arr or ['кожаные', 'тупые']
        
    def generate_phrase(self):
        bastards = np.random.choice(self.phrases_arr)
        return bastards

    def generate_bastards(self):
        bastards = np.random.choice(self.bastards_arr)
        return bastards
        
    def generate_adjective(self):
        bastards = np.random.choice(self.adjective_arr)
        return bastards
        
    def replace_templates(self, msg):
        logger.debug('replacing all templates in msg: {}'.format(msg))
        msg = _replace_all_occurrences(msg, '{phrase}', self.generate_phrase)
        msg = _replace_all_occurrences(msg, '{bast}', self.generate_bastards)
        msg = _replace_all_occurrences(msg, '{adj}', self.generate_adjective)
        logger.debug('after replacing templates: {}'.format(msg))
        return msg
    
    def say(self, msg):
        msg = self.replace_templates(msg)
        logger.info('BOT.SAY("{}")'.format(msg))

    def say_all_messages_from_query(self, bot_msg_query, killer=None):
        while True:
            msg = bot_msg_query.get()
            logger.debug('got new msg from query: {}'.format(msg))
            if killer is not None and killer.kill_now:
                logger.info('kill_now is True - stopping bot say_all_messages loop.')
                break
            self.say(msg)
            bot_msg_query.task_done()
